// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Header from './components/Header'
import List from './components/List'

import DataTables from 'vue-data-tables'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(ElementUI)
Vue.use(DataTables)
var VueMaterial = require('vue-material')
Vue.use(VueMaterial)

Vue.material.registerTheme('default', {
  primary: {
    color: 'teal',
    hue: '500'
  },
  accent: 'black',
  warn: 'black',
  background: 'black'
})

Vue.config.productionTip = false
/* eslint-disable no-new */

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})

new Vue({
  el: '#header',
  template: '<Header/>',
  components: { Header }
})

new Vue({
  el: '#list',
  template: '<List/>',
  components: { List }
})
